package dto;

public class Serie {
	private final int defNumeroTempo = 3;
	private final boolean defEntregado = false;

	private String titulo;
	private int numeroTempo;
	private boolean entregado;
	private String genero;
	private String creador;

	public Serie() {
		this.numeroTempo = defNumeroTempo;
		this.entregado = defEntregado;
		this.titulo = "";
		this.genero = "";
		this.creador = "";
	}

	
	
	public Serie(String titulo, String creador) {
		super();
		this.numeroTempo = defNumeroTempo;
		this.entregado = defEntregado;
		this.titulo = titulo;
		this.genero = "";
		this.creador = creador;
	}



	public Serie(String titulo, int numeroTempo, String generp, String creador) {
		super();
		this.titulo = titulo;
		this.numeroTempo = numeroTempo;
		this.entregado = defEntregado;
		this.genero = generp;
		this.creador = creador;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public int getNumeroTempo() {
		return numeroTempo;
	}



	public void setNumeroTempo(int numeroTempo) {
		this.numeroTempo = numeroTempo;
	}



	public boolean isEntregado() {
		return entregado;
	}



	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}



	public String getGenerp() {
		return genero;
	}



	public void setGenerp(String generp) {
		this.genero = generp;
	}



	public String getCreador() {
		return creador;
	}



	public void setCreador(String creador) {
		this.creador = creador;
	}



	public int getDefNumeroTempo() {
		return defNumeroTempo;
	}



	public boolean isDefEntregado() {
		return defEntregado;
	}



	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTempo=" + numeroTempo + ", entregado=" + entregado + ", generp=" + genero + ", creador="
				+ creador + "]";
	}
	
	
	
}
