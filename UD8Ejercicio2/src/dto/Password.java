package dto;

import java.util.Random;

public class Password {

	private String contrase�a;
	private int longitud;


	public Password() {
		this.longitud = 8;
		this.contrase�a = generatePasswd();

	}


	public Password(int longitud) {
		this.longitud = longitud;
		this.contrase�a = generatePasswd();
	}

	private String generatePasswd() {
		Random random = new Random();
		String passwd = "";
		String caracters ="qwertyuiopasdfghjkl�zxcvbnmQWWERTYUIOPASDFGHJKL�ZXCVBNM1234567890";
		for (int i = 0; i < longitud; i++) {
			passwd+= caracters.charAt(random.nextInt(caracters.length()));
		}

		return passwd;
	}


	@Override
	public String toString() {
		return "Password [contrase�a=" + contrase�a + "]";
	}
	
	

}
