package dto;

import java.util.Enumeration;

import com.sun.jmx.snmp.Enumerated;

public class Electrodomestico {

	private final double defPrecioBase = 100;
	private final String defColor = "Blanco";
	private final char defConsumoEner = 'F';
	private final double defPeso = 5;

	protected double precioBase;
	protected String color;
	protected char consumoEner;
	protected double peso;


	public Electrodomestico() {
		super();
		this.precioBase = defPrecioBase;
		this.color = defColor;
		this.consumoEner = defConsumoEner;
		this.peso = defPeso;
	}

	public Electrodomestico(double precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = defColor;
		this.consumoEner = defConsumoEner;
		this.peso = peso;
	}

	public Electrodomestico(double precioBase, String color, char consumoEner, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = checkColor(color);
		this.consumoEner = chckEner(consumoEner);
		this.peso = peso;
	}

	private String checkColor(String chkColor) {
		chkColor = chkColor.toUpperCase();

		switch (chkColor) {
		case "blanco":
			return "Blanco";
		case "negro":
			return "Negro";

		case "rojo":
			return "Rojo";

		case "azul":
			return "Azul";

		case "gris":
			return "Gris";

		default:
			System.out.println("Color no valido (Por defecto se ha puesto el Blanco)");
			return "Blanco";
		}

	}

	private char chckEner(char chkEner) {

		switch (chkEner) {
		case 'A':
			return 'A';
		case 'B':
			return 'B';

		case 'C':
			return 'C';

		case 'D':
			return 'D';

		case 'E':
			return 'E';
		case 'F':
			return 'F';
			
		default:
			System.out.println("Consumo energetico no valido (Por defecto se ha puesto la letra F)");
			return 'F';
		}

	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color
				+ ", consumoEner=" + consumoEner + ", peso=" + peso + "]";
	}
	
	


}
