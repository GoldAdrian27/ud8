import java.util.Scanner;

import dto.Electrodomestico;

public class mainApp {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		Electrodomestico elec = new Electrodomestico(100, "amarillo", 'G', 53);
		System.out.println(elec);
		
		elec = new Electrodomestico();
		System.out.println(elec);
		
		elec = new Electrodomestico(100, 53);
		System.out.println(elec);
	}

}
